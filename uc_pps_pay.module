<?php
// $Id:$

/**
 * @file
 * Process payments using Pinnacle Payment Solutions.
 *
 * Code by Adam A. Gregory - http://AdamAGregory.com
 *
 * Development sponsored by:
 * * Polish Your Image - http://www.polishyourimage.com/
 *
 */

 define('UC_PPS_PAY_TEST_UN', 'TestTerminal');
 define('UC_PPS_PAY_TEST_PW', 'TestPass');
 define('UC_PPS_PAY_GATEWAY', 'https://www.ppscommerce.net/SmartPayments/transact.asmx');

/**
 * Implementation of hook_payment_gateway().
 */
function uc_pps_pay_payment_gateway() {
  $gateways[] = array(
    'id' => 'pps_pay',
    'title' => t('Pinnacle Payment Solutions'),
    'description' => t('Process credit card payments using Pinnacle Payment Solutions.'),
    'settings' => 'uc_pps_pay_settings_form',
    'credit' => 'uc_pps_pay_charge',
    'credit_txn_types' => array(UC_CREDIT_AUTH_ONLY, UC_CREDIT_PRIOR_AUTH_CAPTURE, UC_CREDIT_AUTH_CAPTURE),
  );

  return $gateways;
}

/**
 * Payment gateway settings form.
 */
function uc_pps_pay_settings_form() {
  $form['uc_pps_pay_api_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable test mode.'),
    '#description' => t('In test mode, transactions will be processed as normal but cards will not be charged.'),
    '#default_value' => variable_get('uc_pps_pay_api_test', FALSE),
  );
  $form['uc_pps_pay_test_cc_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test Credit Card Numbers'),
    '#collapsible' => TRUE,
    '#collapsed' => !variable_get('uc_pps_pay_api_test', FALSE),
  );
  $form['uc_pps_pay_test_cc_info']['cc'] = array(
    '#type' => 'markup',
    '#value' => theme('table',array('Card Type', 'Number', 'CVV Code'), array(
      array('MasterCard', '5424000000000015', '998'),
      array('Visa','4788250000028291','999'),
      array('Discover','601100000000012','996'),
      array('American Express','370000000000002','9997'),
      )),
  );
  $form['uc_pps_pay_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('pps_pay user name'),
    '#default_value' => variable_get('uc_pps_pay_api_username', ''),
    '#required' => TRUE,
  );
  $form['uc_pps_pay_api_password'] = array(
    '#type' => 'password',
    '#title' => t('pps_pay password'),
    '#default_value' => variable_get('uc_pps_pay_api_password', ''),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Callback for processing a credit card transaction.
 */
function uc_pps_pay_charge($order_id, $amount, $data) {
  global $user;
  $order = uc_order_load($order_id);

  // Build POST data for the transaction.
  $submit_data['Amount'] = uc_currency_format($amount, FALSE, FALSE, '.');

  if ($data['txn_type'] == UC_CREDIT_PRIOR_AUTH_CAPTURE) {
    // Capture previous authorization using transaction ID.
    $submit_data['TransType'] = 'Capture';
    $submit_data['PNRef'] = $data['auth_id'];
  }
  else {
    // Sale or authorization request.
    $submit_data['TransType'] = ($data['txn_type'] == UC_CREDIT_AUTH_ONLY) ? 'Auth' : 'Sale';
    $submit_data['CardNum'] = $order->payment_details['cc_number'];
    $submit_data['ExpDate'] = substr('0'. $order->payment_details['cc_exp_month'], -2).substr($order->payment_details['cc_exp_year'], -2);
    $submit_data['PNRef'] = '';
    $submit_data['CVNum'] = '';
    // Add card CSC/CVV value, if enabled.
    if (variable_get('uc_credit_cvv_enabled', TRUE)) {
      $submit_data['CVNum'] = $order->payment_details['cc_cvv'];
    }

    $submit_data['InvNum'] = $order_id;

    // Calculate tax.
    $tax = 0;
    if (module_exists('uc_taxes')) {
      foreach (uc_taxes_calculate($order) as $tax_item) {
        $tax += $tax_item->amount;
      }
    }

    // Optional billing address fields
    $submit_data['NameOnCard'] = substr($order->billing_first_name .' '. $order->billing_last_name, 0, 50);
    $submit_data['Street'] = substr($order->billing_street1, 0, 50);
    $submit_data['Zip'] = substr($order->billing_postal_code, 0, 9);

  }

  // Send transaction request.
  $response = _uc_pps_pay_send_request('ProcessCreditCard', $submit_data);

  if (!$response) {
    return array('success' => FALSE);
  }

  // Check for errors.
  $success = FALSE;
  if ($response->Result != 0) {
    $message = t('Pinnacle Payment error: @error', array('@error' => $response->RespMSG));
  }
  else {
    if ($data['txn_type'] == UC_CREDIT_PRIOR_AUTH_CAPTURE) {
      // For a capture, check for correct response code.
      $success = ($response->Result != 0); // "112. Your transaction was successfully captured."
      if (!$success) {
        $message = t('Capture failed: @error', array('@error' => $response->RespMSG));
      }
      else {
        $message = t('Capture successful.<br />Transaction ID: @id', array('@id' => $response->PNRef));
        uc_credit_log_prior_auth_capture($order_id, $data['auth_id']);
      }
    }
    elseif (!$response->AuthCode) {
      // No approval code means the card was declined.
      $message = t('Credit card payment declined: @error', array('@error' => $response->RespMSG));
    }
    else {
      // Sale or authorization succeeded.
      $message = t('Message: @message<br />Transaction ID: @id', array('@message' => $response->RespMSG, '@id' => $response->PNRef));
      $success = TRUE;

      // Log authorization data.
      if ($data['txn_type'] == UC_CREDIT_AUTH_ONLY) {
        uc_credit_log_authorization($order_id, $response->PNRef, $amount);
      }
    }
  }

  // Build result array.
  $result = array(
    'success' => $success,
    'comment' => $message,
    'message' => $message,
    'uid' => $user->uid,
    'log_payment' => ($data['txn_type'] != UC_CREDIT_AUTH_ONLY),
  );
  if ($response->PNRef) {
    $result['data'] = array('module' => 'uc_pps_pay', 'txn_id' => $response->PNRef);
  }

  // Build an admin order comment.
  $comment = $message .'<br />'. t('Auth code: @avs', array('@avs' => $response->AuthCode));

  uc_order_comment_save($order_id, $user->uid, $comment, 'admin');

  return $result;
}

/**
 * Send a request to the pps_pay API and parse the response.
 */
function _uc_pps_pay_send_request($op, $data) {
  // Add common fields to the request.
  $data = array(
    'UserName' => variable_get('uc_pps_pay_api_username', ''),
    'Password' => variable_get('uc_pps_pay_api_password', ''),
    'ExtData' => '<EntryMode>Unknown</EntryMode>',
    'MagData' => '',
  ) + $data;

  if (variable_get('uc_pps_pay_api_test', FALSE)) {
    $data['UserName'] = UC_PPS_PAY_TEST_UN;
    $data['Password'] = UC_PPS_PAY_TEST_PW;
  }

  $url = UC_PPS_PAY_GATEWAY.'/'.$op;

  foreach($data as $k => $v) {
    $post_data .= $k .'='. $v.'&';
  }
  $post_data = rtrim($post_data, '&');
  // Send the cURL request and retrieve the response.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
  $response_string = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('uc_pps_pay', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return FALSE;
  }

  curl_close($ch);

  // Parse the response string.
  $response = simplexml_load_string($response_string);

  return $response;
}
